   _____                     _      _                _  
  / ____|                   | |    | |              | | 
 | (___    ___  _ __  _   _ | |__  | |__    ___   __| | 
  \___ \  / __|| '__|| | | || '_ \ | '_ \  / _ \ / _` | 
  ____) || (__ | |   | |_| || |_) || |_) ||  __/| (_| | 
 |_____/  \___||_|    \__,_||_.__/ |_.__/  \___| \__,_| 
 __  __             _         _       
|  \/  |           | |       | |      
| \  / |  ___    __| | _   _ | |  ___ 
| |\/| | / _ \  / _` || | | || | / _ \
| |  | || (_) || (_| || |_| || ||  __/
|_|  |_| \___/  \__,_| \__,_||_| \___|
   _____              _                     __    __  ______  __                              
  / ____|            | |                   /_ |  /_ ||____  |/_ |                             
 | (___   _   _  ___ | |_  ___  _ __ ___    | |   | |    / /  | |                             
  \___ \ | | | |/ __|| __|/ _ \| '_ ` _ \   | |   | |   / /   | |                             
  ____) || |_| |\__ \| |_|  __/| | | | | |  | | _ | |  / /    | |                             
 |_____/  \__, ||___/ \__|\___||_| |_| |_|  |_|(_)|_| /_/     |_|                             
           __/ |                                                                              
          |___/                                                                               


Originally created for 1.134 by the incredibly patient and legendary Theoris.

Updated to 1.171 by SupaNinjaMan

# ################################################################################ #

Also included:
	The cleaned, reorganized and heavily commented header_operations made by the man, the myth, the legend Lav!

So what did you do, dstn/Sionfel/SupaNinjaMan? Well I:
	Updated to code to include ALL engine updates since 1.134.
	Erased more of the non-essential code, making it almost truly barebones.
	Updated module.ini that adds all new engine variables and removes non essential BRFs from the loading order
	Readded and expanded the headers from TW to tell you how to use the MS
	Added missing headers for files that never had them
	Organized various entries like module_sounds by category
	Did this Readme. . . . 
	. . .
	I guess I went through and made the structure as consistent as I was willing to.
	Added really useless irreverent comments every ten seconds for my own amusement.
	. . .
	I probably did something else. Don't look at me.

# ################################################################################ #

What are the potential uses for this?

	This is a pretty user-unfriendly framework. Essentially it's for optimization demons who need nothing from Native and have the knowledge to bring over the things they do need. This framework is also devoid of nearly all MP related code, so I would probably recommend using a different MS as a base unless you're experienced enough to bring over the MP features from Native from zero.

# ################################################################################ #

Should I use this?

	Probably not, not in most cases at least. But for advanced users, it is a great way to trim the fat of unnecessary features.
	
# ##### Original Readme.txt included in 1.171 ################################### #

Mount&Blade Module System

For getting documentation and the latest version of the module system check out:

www.taleworlds.com/mb_module_system.html

# ################################################################################ #

ASCII Art was generated on http://patorjk.com/software/taag/ using
Big by Glenn Chappell 4/93