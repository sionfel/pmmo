from ID_items import *
from ID_quests import *
from ID_factions import *

# #######################################################################
#	constant_id = constant_value
#
#		These constants are used in various files.
#		If you need to define a value that will be used in those files,
#		just define it here rather than copying it across each file, so
#		that it will be easy to change it if you need to.
#
#		for example, declaring example_num = 256 will then make it so
#		when you type example_num in any file with module_constants
#		in the import will make example_num become 256
#
#	The other, much more common use, is slots.
#	slot_id = slot_number
#
#		By using slots you can use operations to store/recieve
#		a value stored in the slot for such things as items, agents
#		factions, quests, parties, etc and so forth.
#
#		Ideally you'll use a different number per slot so as 
#		not to accidentally overwrite previously assigned slots, but 
#		things like items and scenes won't be sharing slot information,
#		so feel free to reset your numbering scheme per category.
#
#		The slots left in are purely for segmentation and categorization
#		efforts are are in no way REQUIRED. They're just to be helpful.
# #######################################################################

# #######################################################################
# 		Item Slots
# #######################################################################

item_slots = 0

item_is_tradable 	= 1
item_is_stackable 	= 2
item_is_noted 		= 3
item_is_notable 	= 4
#item_is_stackable 	= 5
item_cost 			= 6
item_lowalch 		= 7
item_highalch 		= 8
item_is_quest_item 	= 9

item_attack_stab 	= 10
item_attack_slash 	= 11
item_attack_crush	= 12
item_attack_magic	= 13
item_attack_ranged 	= 14
item_defence_stab	= 15
item_defence_slash 	= 16
item_defence_crush 	= 17
item_defence_magic 	= 18
item_defence_ranged = 19
item_melee_strength = 20
item_ranged_strength = 21
item_magic_damage 	= 22
item_equipment_slot = 23
item_attack_speed 	= 24
item_skill_reqs 	= 25

# #######################################################################
# 		Agent Slots
# #######################################################################

agent_slots = 0

ai_agent_activity 					= 1
ai_agent_aggressiveness 			= 2
ai_agent_time_since_last_action 	= 3
ai_agent_target						= 4
ai_agent_backup						= 5

# #######################################################################
# 		Faction Slots
# #######################################################################

faction_slots = 0

# #######################################################################
# 		Party Slots
# #######################################################################

party_slots = 0


# #######################################################################
# 		Scene Slots
# #######################################################################

scene_slots = 0


# #######################################################################
# 		Troop Slots
# #######################################################################

troop_slots = 0

troop_skill_attack_level 		= 1
troop_skill_strength_level 		= 2
troop_skill_defence_level 		= 3
troop_skill_ranged_level 		= 4
troop_skill_prayer_level 		= 5
troop_skill_magic_level 		= 6
troop_skill_runecraft_level 	= 7
troop_skill_hitpoints_level 	= 8
troop_skill_crafting_level 		= 9
troop_skill_mining_level 		= 10
troop_skill_smithing_level 		= 11
troop_skill_fishing_level 		= 12
troop_skill_cooking_level 		= 13
troop_skill_firemaking_level 	= 14
troop_skill_woodcutting_level 	= 15
troop_skill_agility_level 		= 16
troop_skill_herblore_level 		= 17
troop_skill_thieving_level 		= 18
troop_skill_fletching_level 	= 19
troop_skill_slayer_level 		= 20
troop_skill_farming_level 		= 21
troop_skill_construction_level 	= 22
troop_skill_hunter_level 		= 23

troop_skill_attack_exp 			= 24
troop_skill_strength_exp 		= 25
troop_skill_defence_exp 		= 26
troop_skill_ranged_exp 			= 27
troop_skill_prayer_exp 			= 28
troop_skill_magic_exp 			= 29
troop_skill_runecraft_exp 		= 30
troop_skill_hitpoints_exp 		= 31
troop_skill_crafting_exp 		= 32
troop_skill_mining_exp 			= 33
troop_skill_smithing_exp 		= 34
troop_skill_fishing_exp 		= 35
troop_skill_cooking_exp 		= 36
troop_skill_firemaking_exp 		= 37
troop_skill_woodcutting_exp 	= 38
troop_skill_agility_exp 		= 39
troop_skill_herblore_exp 		= 40
troop_skill_thieving_exp 		= 41
troop_skill_fletching_exp 		= 42
troop_skill_slayer_exp 			= 43
troop_skill_farming_exp 		= 44
troop_skill_construction_exp 	= 45
troop_skill_hunter_exp 			= 46

troop_head_slot 				= 47
troop_cape_slot 				= 48
troop_neck_slot 				= 49
troop_ammunition_slot 			= 50 
troop_weapon_slot 				= 51
troop_shield_slot 				= 52
troop_body_slot 				= 53
troop_legs_slot 				= 54
troop_hands_slot 				= 55
troop_feet_slot 				= 56
troop_ring_slot 				= 57
troop_jaw_slot 					= 58

troop_party_id					= 59
troop_supports					= 60

# #######################################################################
# 		Player Slots
# #######################################################################

player_slots = 0

# #######################################################################
# 		Team Slots
# #######################################################################

team_slots = 0

# #######################################################################
# 		Quest Slots
# #######################################################################

quest_slots = 0

# #######################################################################
# 		Party Template Slots
# #######################################################################

party_template_slots = 0

# #######################################################################
# 		Scene Prop Slots
# #######################################################################

scene_prop_slots = 0

# #######################################################################
#		Miscellaneous
# #######################################################################
miscellaneous = 0

activity_wandering 		= 0
activity_fighting		= 1
activity_hunting		= 2
activity_woodcutting	= 3
activity_fishing		= 4
activity_farming		= 5
activity_patrolling		= 6
activity_guarding		= 7
activity_vendor			= 8

# #######################################################################
#		Constants
# #######################################################################

vendors_begin = "trp_blacksmith_00"
blacksmith_begin = vendors_begin
foodseller_begin = "trp_foodseller_00"
weaponseller_begin = "trp_weaponseller_00"
accessseller_begin = "trp_accessseller_00"
potionseller_begin = "trp_potionseller_00"
shadyseller_begin = "trp_shadyseller_00"
players_begin = "trp_npc_000"
mobs_begin = "trp_monster_000"

player_parties_begin = "p_party_000"
player_parties_end = "p_parties_end"

vendors_end = players_begin
blacksmith_end = foodseller_begin
foodseller_end = weaponseller_begin
weaponseller_end = accessseller_begin
accessseller_end = potionseller_begin
potionseller_end = shadyseller_begin
shadyseller_end = players_begin
players_end = mobs_begin
mobs_end = "trp_end_troops"