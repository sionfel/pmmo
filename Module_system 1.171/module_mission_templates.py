from header_common import *
from header_operations import *
from header_mission_templates import *
from header_animations import *
from header_sounds import *
from header_music import *
from header_items import *
from module_constants import *

# #######################################################################
#   Each mission-template is a tuple that contains the following fields:
#  1) Mission-template id (string): used for referencing mission-templates in other files.
#     The prefix mt_ is automatically added before each mission-template id
#
#  2) Mission-template flags (int): See header_mission-templates.py for a list of available flags
#  3) Mission-type(int): Which mission types this mission template matches.
#     For mission-types to be used with the default party-meeting system,
#     this should be 'charge' or 'charge_with_ally' otherwise must be -1.
#     
#  4) Mission description text (string).
#  5) List of spawn records (list): Each spawn record is a tuple that contains the following fields:
#    5.1) entry-no: Troops spawned from this spawn record will use this entry
#    5.2) spawn flags.
#    5.3) alter flags. which equipment will be overriden
#    5.4) ai flags.
#    5.5) Number of troops to spawn.
#    5.6) list of equipment to add to troops spawned from here (maximum 8).
#  6) List of triggers (list).
#     See module_triggers.py for infomation about triggers.
# #######################################################################

label_presentation = (0, 0, 0, [(neg|is_presentation_active, "prsnt_display_agent_labels")], [(start_presentation, "prsnt_display_agent_labels"),])

spawn_trigger_town = (ti_after_mission_start, 0, 0, [],
       [
		 (set_fixed_point_multiplier, 100),
         (store_random_in_range, ":standers", 75, 251),
		 
		 (get_player_agent_no, ":agent"),
		 (agent_get_position, pos10, ":agent"),
		 
		 (scene_prop_get_num_instances, ":max", "spr_player_spawner"),
		 
		 # Spawns player agents
		 
		 (try_for_range, ":i", 0, ":standers"),
			(store_random_in_range, ":player_id", players_begin, players_end),
			
			(store_random_in_range, ":s", 0, ":max"),
			(scene_prop_get_instance, ":s_id", "spr_player_spawner", ":s"),
			(prop_instance_get_position, pos10, ":s_id"),
			(set_spawn_position, pos10),
			
			(spawn_agent, ":player_id"),
			(agent_get_position, pos10, reg0),
			(store_random_in_range, ":var_x", -250, 250),
			(store_random_in_range, ":var_y", -250, 250),
			
			(position_move_x, pos10, ":var_x"),
			(position_move_y, pos10, ":var_y"),
			(agent_set_position, reg0, pos10),
		 (try_end),		 
		 
		 #  Spawns Merchants
		 
		 (try_for_prop_instances, ":i", "spr_merchant_spawner"),
			(prop_instance_get_position, pos10, ":i"),
			(prop_instance_get_variation_id, ":merchant_type", ":i"),
			(prop_instance_get_variation_id_2, ":merchant_offset", ":i"),
			(set_spawn_position, pos10),
			
			(try_begin),
				(eq, ":merchant_offset", 0),
				(store_random_in_range, ":merchant_offset", 0, 1000),
			(try_end),
			
			(try_begin),
				(eq, ":merchant_type", 0), # Blacksmith
				(store_sub, ":offset_mod", blacksmith_end, blacksmith_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", blacksmith_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Blacksmith {s10}"),
			(else_try),
				(eq, ":merchant_type", 1), # Food Vendor
				(store_sub, ":offset_mod", foodseller_end, foodseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", foodseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Food Vendor {s10}"),
			(else_try),
				(eq, ":merchant_type", 2), # Weapon Seller
				(store_sub, ":offset_mod", weaponseller_end, weaponseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", weaponseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Weapon Seller {s10}"),
			(else_try),
				(eq, ":merchant_type", 3), # Accessories Seller
				(store_sub, ":offset_mod", accessseller_end, accessseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", accessseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Accessories Seller {s10}"),
			(else_try),
				(eq, ":merchant_type", 4), # Potions Seller
				(display_message, "@I was spawned, Potions"),
				(store_sub, ":offset_mod", potionseller_end, potionseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", potionseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Potions Seller {s10}"),
			(else_try),
				(eq, ":merchant_type", 4), # Blackmarket
				(store_sub, ":offset_mod", shadyseller_end, shadyseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", shadyseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Blackmarket {s10}"),
			(try_end),
		 (try_end),
	])

spawn_trigger_adventure = (ti_after_mission_start, 0, 0, [],
       [
		 (set_fixed_point_multiplier, 100),
         (store_random_in_range, ":parties", 4, 11),
		 
		 (get_player_agent_no, ":agent"),
		 (agent_get_position, pos10, ":agent"),
		 
		 (scene_prop_get_num_instances, ":max", "spr_player_spawner"),
		 
		 # Spawns player agents
		 
		 (try_for_range, ":i", 0, ":parties"),
			(store_random_in_range, ":party_id", player_parties_begin, player_parties_end),
			
			(store_random_in_range, ":s", 0, ":max"),
			(scene_prop_get_instance, ":s_id", "spr_player_spawner", ":s"),
			(prop_instance_get_position, pos10, ":s_id"),
			(set_spawn_position, pos10),
			
			(party_get_num_companions, ":count", ":party_id"),
			(try_for_range, ":member", 0, ":count"),
				(party_stack_get_troop_id, ":player", ":party_id", ":member"),
				(spawn_agent, ":player"),
				(agent_get_position, pos11, reg0),
				(store_random_in_range, ":var_x", -250, 250),
				(store_random_in_range, ":var_y", -250, 250),
				
				(position_move_x, pos11, ":var_x"),
				(position_move_y, pos11, ":var_y"),
				(agent_set_position, reg0, pos11),
			(try_end),
			
		 (try_end),		 
		 
		 #  Spawns Merchants
		 
		 (try_for_prop_instances, ":i", "spr_merchant_spawner"),
			(prop_instance_get_position, pos10, ":i"),
			(prop_instance_get_variation_id, ":merchant_type", ":i"),
			(prop_instance_get_variation_id_2, ":merchant_offset", ":i"),
			(set_spawn_position, pos10),
			
			(try_begin),
				(eq, ":merchant_offset", 0),
				(store_random_in_range, ":merchant_offset", 0, 1000),
			(try_end),
			
			(try_begin),
				(eq, ":merchant_type", 0), # Blacksmith
				(store_sub, ":offset_mod", blacksmith_end, blacksmith_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", blacksmith_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Blacksmith {s10}"),
			(else_try),
				(eq, ":merchant_type", 1), # Food Vendor
				(store_sub, ":offset_mod", foodseller_end, foodseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", foodseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Food Vendor {s10}"),
			(else_try),
				(eq, ":merchant_type", 2), # Weapon Seller
				(store_sub, ":offset_mod", weaponseller_end, weaponseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", weaponseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Weapon Seller {s10}"),
			(else_try),
				(eq, ":merchant_type", 3), # Accessories Seller
				(store_sub, ":offset_mod", accessseller_end, accessseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", accessseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Accessories Seller {s10}"),
			(else_try),
				(eq, ":merchant_type", 4), # Potions Seller
				(display_message, "@I was spawned, Potions"),
				(store_sub, ":offset_mod", potionseller_end, potionseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", potionseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Potions Seller {s10}"),
			(else_try),
				(eq, ":merchant_type", 4), # Blackmarket
				(store_sub, ":offset_mod", shadyseller_end, shadyseller_begin),
				(val_mod, ":merchant_offset", ":offset_mod"),
				(val_add, ":merchant_offset", shadyseller_begin),
				(spawn_agent, ":merchant_offset"),
				(str_store_agent_name, s10, reg0),
				(display_message, "@I was spawned, Blackmarket {s10}"),
			(try_end),
		 (try_end),
		 
		 # Spawn Enemies
		 
		  (try_for_prop_instances, ":i", "spr_enemy_spawner"),
			(prop_instance_get_position, pos10, ":i"),
			(prop_instance_get_variation_id, ":troop_id", ":i"),
			(prop_instance_get_variation_id_2, ":count", ":i"),
			(val_add, ":troop_id", mobs_begin),
			
			(gt, ":count", 0),
			(val_add, ":count", 1),
			
			
			(try_for_range, ":i", 0, ":count"),
				(copy_position, pos11, pos10),
				(set_spawn_position, pos11),
			
				(spawn_agent, ":troop_id"),
				(agent_get_position, pos11, reg0),
				(store_random_in_range, ":var_x", -250, 250),
				(store_random_in_range, ":var_y", -250, 250),
				
				(position_move_x, pos10, ":var_x"),
				(position_move_y, pos10, ":var_y"),
				(agent_set_position, reg0, pos11),
			(try_end),
			
		 (try_end),
 ])

mob_respawn = (3, 0, 0, [], 
	[
		(set_fixed_point_multiplier, 100),
		(try_for_prop_instances, ":i", "spr_enemy_spawner"),
			(prop_instance_get_position, pos10, ":i"),
			(prop_instance_get_variation_id, ":troop_id", ":i"),
			(prop_instance_get_variation_id_2, ":count", ":i"),
			(val_add, ":troop_id", mobs_begin),
			
			(try_for_agents, ":agent", pos10, 1000),
				(agent_is_alive, ":agent"),
				(agent_get_troop_id, ":a_troop", ":agent"),
				(eq, ":a_troop", ":troop_id"),
				(val_sub, ":count", 1),
			(try_end),
			
			(gt, ":count", 0),
							
			(store_random_in_range, ":var_x", -750, 750),
			(store_random_in_range, ":var_y", -750, 750),
			
			(position_move_x, pos10, ":var_x"),
			(position_move_y, pos10, ":var_y"),
			
			(set_spawn_position, pos10),
		
			(spawn_agent, ":troop_id"),
			
		 (try_end),
	])

npc_ai_rethink = (1, 0, 0, [], 
	[
		(try_for_agents, ":agent"),
			(agent_is_non_player, ":agent"),
			(agent_is_human, ":agent"),
			(agent_get_slot, ":activity", ":agent", ai_agent_activity),
			(agent_get_slot, ":target", ":agent", ai_agent_target),
			(agent_get_position, pos1, ":agent"),
			(agent_get_scripted_destination, pos3, ":agent"),
			(agent_get_troop_id, ":troop", ":agent"),
			
			(set_fixed_point_multiplier, 100),
			
			(try_begin),
				(ge, ":troop", mobs_begin),
				
				(try_for_agents, ":i", pos1, 250),
					(neq, ":i", ":agent"),
					(agent_get_troop_id, ":i_troop", ":i"),
					(try_begin),
						(lt, ":i_troop", mobs_begin),
						
						(agent_get_slot, ":aggro", ":agent", ai_agent_aggressiveness),
				
						(agent_add_relation_with_agent, ":agent", ":i", -1),
						(agent_add_relation_with_agent, ":i", ":agent", -1),
						
						(agent_set_slot, ":agent", ai_agent_aggressiveness, 1),
						(agent_set_slot, ":agent", ai_agent_target, ":i"),
						(agent_set_slot, ":agent", ai_agent_activity, activity_fighting),
						(agent_ai_set_aggressiveness, ":agent", 100),
						
						(eq, ":aggro", 0),
						
						(agent_clear_scripted_mode, ":agent"),
						(agent_force_rethink, ":agent"),
					(try_end),
				(try_end),
				
			(else_try),
				(is_between, ":troop", players_begin, players_end),
				
				(try_begin),
					(eq, ":activity", activity_wandering),
					
					(try_begin),
						(eq, ":target", -1),
						
						(scene_prop_get_num_instances, ":max", "spr_point_of_interest"),
						(store_random_in_range, ":poi", 0, ":max"),
						
						(agent_set_slot, ":agent", ai_agent_target, ":poi"),
						
						(scene_prop_get_instance, ":poi_id", "spr_point_of_interest", ":poi"),
						(prop_instance_get_position, pos2, ":poi_id"),
						
						(agent_set_scripted_destination, ":agent", pos2),
						(agent_force_rethink, ":agent"),
					(else_try),
						(scene_prop_get_instance, ":poi_id", "spr_point_of_interest", ":target"),
						(prop_instance_get_position, pos2, ":poi_id"),
						
						(get_distance_between_positions_in_meters, ":d", pos1, pos2),
						
						(try_begin),
							(lt, ":d", 2),
							(scene_prop_get_num_instances, ":max", "spr_point_of_interest"),
							(store_random_in_range, ":poi", 0, ":max"),
							(agent_set_slot, ":agent", ai_agent_target, ":poi"),
							(scene_prop_get_instance, ":poi_id", "spr_point_of_interest", ":poi"),
							(prop_instance_get_position, pos2, ":poi_id"),
						(try_end),
						
						(get_distance_between_positions_in_meters, ":dt", pos2, pos3),
						(gt, ":dt", 1),
						
						(agent_set_scripted_destination, ":agent", pos2),
						(agent_force_rethink, ":agent"),
					(try_end),
					
				(else_try),
					(eq, ":activity", activity_fighting),
					
				(try_end),
			(try_end),
			
		(try_end),
	])

agent_spawn_trigger = (ti_on_agent_spawn, 0, 0, [], 
	[
		(store_trigger_param_1, ":agent"),
		(get_player_agent_no, ":p_agent"),
		(agent_get_troop_id, ":troop", ":agent"),
		
		(try_begin),
			(eq, ":agent", ":p_agent"),
			(agent_equip_item, ":agent", "itm_sledgehammer"),
			(agent_set_wielded_item, ":agent", "itm_sledgehammer"),
		(else_try),
			(agent_set_team, ":agent", 1),
			(agent_add_relation_with_agent, ":agent", ":p_agent", 1),
		(try_end),
		
		
		(try_begin),
			(is_between, ":troop", players_begin, players_end),
			(try_begin),
				(agent_ai_set_interact_with_player, ":agent", 0),
				(agent_set_slot, ":agent", ai_agent_target, -1),
				(agent_set_slot, ":agent", ai_agent_activity, activity_wandering),
				
				(gt, ":p_agent", 0),
				(neq, ":p_agent", ":agent"),
				
				(store_random_in_range, ":i", 0, 25),
				
				(try_begin),
					(ge, ":i", 22),
					(store_random_in_range, ":i", 2, 6),
					(store_mul, ":move_speed", ":i", 5),
				(else_try),
					(le, ":i", 3),
					(assign, ":move_speed", 1),
				(else_try),
					(assign, ":move_speed", 5),
				(try_end),
					
				(agent_set_speed_limit, ":agent", ":move_speed"),
			(try_end),
		(else_try),
			(is_between, ":troop", vendors_begin, vendors_end),
			(try_begin),
				(agent_set_slot, ":agent", ai_agent_target, -1),
				(agent_set_slot, ":agent", ai_agent_activity, activity_vendor),
				(agent_set_team, ":agent", 1),
				
				(gt, ":p_agent", 0),
				(neq, ":p_agent", ":agent"),
				
				(agent_add_relation_with_agent, ":agent", ":p_agent", 1),
			(try_end),
		(else_try),
			(ge, ":troop", mobs_begin),
			(try_begin),
				(agent_set_slot, ":agent", ai_agent_target, -1),
				(agent_set_slot, ":agent", ai_agent_activity, activity_patrolling),
				(agent_clear_scripted_mode, ":agent"),
				(agent_force_rethink, ":agent"),
			(try_end),
		(try_end),
	])

agent_hit_trigger = (ti_on_agent_hit, 0, 0, [], 
	[
		(set_fixed_point_multiplier, 100),
		(store_trigger_param_1, ":agent"),
		(store_trigger_param_2, ":attacker"),
		
		(str_store_agent_name, s1, ":agent"),
		(str_store_agent_name, s2, ":attacker"),
		
		(agent_get_troop_id, ":troop", ":agent"),
		(troop_get_slot, ":party", ":troop", troop_party_id),
		
		(get_player_agent_no, ":p_agent"),
		(agent_get_slot, ":aggro", ":agent", ai_agent_aggressiveness),

		# Party Aggro ###
		(try_begin),
			(ge, ":party", player_parties_begin),
			(is_between, ":troop", players_begin, players_end),
			
			(party_get_num_companions, ":count", ":party"),
			
			(try_for_agents, ":p_member"),
				(agent_get_troop_id, ":p_troop", ":p_member"),
				(try_for_range, ":member", 0, ":count"),
					(party_stack_get_troop_id, ":player", ":party", ":member"),
					(eq, ":player", ":p_troop"),
					
					(agent_add_relation_with_agent, ":p_member", ":attacker", -1),
					(agent_add_relation_with_agent, ":attacker", ":p_member", -1),
					(agent_set_slot, ":p_member", ai_agent_aggressiveness, 1),
					(agent_set_slot, ":p_member", ai_agent_activity, activity_fighting),
					(agent_set_speed_limit, ":p_member", 35),
					(agent_ai_set_aggressiveness, ":p_member", 100),
					
					(agent_clear_scripted_mode, ":p_member"),
					(agent_force_rethink, ":p_member"),
				(try_end),
			(try_end),
		(try_end),
		
		(try_begin),
			(ge, ":troop", mobs_begin),
			(agent_slot_eq, ":agent", ai_agent_backup, 0),
			(assign, ":quit", 0),
			
			(troop_get_slot, ":max_backup", ":troop", troop_supports),
			
			(agent_get_position, pos19, ":agent"),
			
			(call_script, "script_sort_agents_by_distance", ":agent", pos19),
			
			(try_for_range, ":i", 0, reg45),
				(troop_get_slot, ":fellow", "trp_agent_array", ":i"),
				
				(agent_is_alive, ":fellow"),	
				
				(agent_get_troop_id, ":p_troop", ":fellow"),
				(eq, ":troop", ":p_troop"),
				
				(lt, ":quit", ":max_backup"),
				
				(agent_add_relation_with_agent, ":fellow", ":attacker", -1),
				(agent_add_relation_with_agent, ":attacker", ":fellow", -1),
				(agent_set_slot, ":fellow", ai_agent_aggressiveness, 1),
				(agent_set_slot, ":fellow", ai_agent_backup, 1),
				(agent_set_slot, ":fellow", ai_agent_activity, activity_fighting),
				(agent_ai_set_aggressiveness, ":fellow", 100),
				
				(agent_clear_scripted_mode, ":fellow"),
				(agent_force_rethink, ":fellow"),
				(val_add, ":quit", 1),
			(try_end),
			
			# (try_for_agents, ":fellow", 1000),
			# 	(agent_get_troop_id, ":p_troop", ":fellow"),
			# 	(eq, ":troop", ":p_troop"),
			# 	(le, ":quit", 3),
			# 	
			# 	(agent_add_relation_with_agent, ":fellow", ":attacker", -1),
			# 	(agent_add_relation_with_agent, ":attacker", ":fellow", -1),
			# 	(agent_set_slot, ":fellow", ai_agent_aggressiveness, 1),
			# 	(agent_set_slot, ":fellow", ai_agent_backup, 1),
			# 	(agent_set_slot, ":fellow", ai_agent_activity, activity_fighting),
			# 	(agent_ai_set_aggressiveness, ":p_member", 100),
			# 	
			# 	(agent_clear_scripted_mode, ":fellow"),
			# 	(agent_force_rethink, ":fellow"),
			# 	(val_add, ":quit", 1),
			# (try_end),
			(agent_set_slot, ":agent", ai_agent_backup, 1),
		(try_end),
		# ###############
		
		(agent_add_relation_with_agent, ":agent", ":attacker", -1),
		(agent_add_relation_with_agent, ":attacker", ":agent", -1),
		(agent_set_slot, ":agent", ai_agent_aggressiveness, 1),
		(agent_set_slot, ":agent", ai_agent_activity, activity_fighting),
		(agent_ai_set_aggressiveness, ":agent", 100),
		
		(is_between, ":troop", players_begin, players_end),
		(agent_set_speed_limit, ":agent", 35),
		
		(neq, ":agent", ":p_agent"),
		(eq, ":aggro", 0),
		
		(agent_clear_scripted_mode, ":agent"),
		(agent_force_rethink, ":agent"),
		
		(try_begin),
			(eq, ":attacker", ":p_agent"),
			(display_message, "@{s1} will now be aggressive towards you"),
		(else_try),
			(display_message, "@{s1} will now be aggressive towards {s2}"),
		(try_end),
		
	])

debug_cheat_triggers = (0, 0, 0, [], 
	[
		(try_begin),
			(key_clicked, key_middle_mouse_button),
			(display_message, "@Worked!"),
			(get_player_agent_no, ":player"),
			(mission_cam_get_position, pos1, ":player"),
			#(position_move_z, pos1, 120),
			(cast_ray, reg1, pos2, pos1),
			(agent_set_position, ":player", pos2),
		(try_end),
		
		(try_begin),
			(key_clicked, key_v),
			(assign, reg10, 0),
			(try_for_agents, reg0),
				(val_add, reg10, 1),
			(try_end),
			(display_message, "@Agents: {reg10}"),	
		(try_end),
	])

common_triggers = [label_presentation, npc_ai_rethink, agent_spawn_trigger, agent_hit_trigger, mob_respawn]
common_town = [spawn_trigger_town]
common_adventure = [spawn_trigger_adventure]

mission_templates = [

    # #######################################################################
    #		town_default and conversation_encounter are HARDCODED! 
	#	Absolutely DO NOT move. You can modify them, but be cool about it.
	# #######################################################################
    ("town_default", 0, -1,
    "Town Default",
    [
	    (0, mtef_scene_source|mtef_team_0, af_override_horse, 0, 1, []),
    ],     
    [
	   
	   spawn_trigger_town,
		
	] + common_triggers),

    ("conversation_encounter", 0, -1,
    "Conversation Encounter",
    [
	    (0, mtef_visitor_source, af_override_fullhelm, 0, 1, []), 
		(1, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
        (2, mtef_visitor_source, af_override_fullhelm, 0, 1, []), 
		(3, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(4, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(5, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(6, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
        (7, mtef_visitor_source, af_override_fullhelm, 0, 1, []), 
		(8, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(9, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(10, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(11, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		
        # prisoners now...
        (12, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(13, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(14, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(15, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(16, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
    
	    # Other party
        (17, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(18, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(19, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(20, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(21, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(22, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(23, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(24, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(25, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(26, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(27, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(28, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(29, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(30, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(31, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
    ],
    [],
    ),
  
    # #######################################################################
	#	I left lead_charge in because it is so commonly used, but it's not
	#						~ R E Q U I R E D ~ 
	#										y'know.
	# #######################################################################

    ("lead_charge", mtf_battle_mode, charge,
    "Lead Charge",
    [
        (1, mtef_defenders|mtef_team_0, 0, aif_start_alarmed, 12, []),
        (0, mtef_defenders|mtef_team_0, 0, aif_start_alarmed, 0, []),
        (4, mtef_attackers|mtef_team_1, 0, aif_start_alarmed, 12, []),
        (4, mtef_attackers|mtef_team_1, 0, aif_start_alarmed, 0, []),
    ],
    [
    ]),
	
	# #######################################################################
	#	Feel free to plug away here.
	# #######################################################################
 
    ("scene_edit", 0, -1,
    "Edit Scenes",
    [
        (0, mtef_scene_source|mtef_team_0, af_override_horse, 0, 1, []),
    ],
    [
		(ti_tab_pressed, 0, 0, [], [
			(finish_mission, 0),
			(jump_to_menu, "mnu_edit_scene_2"),
		]),
	
    ]),
	
	("mmo_town", 0, -1,
    "Explore Town",
    [
        (0, mtef_scene_source|mtef_team_0, af_override_horse, 0, 1, []),
    ],
    [
		(ti_tab_pressed, 0, 0, [], [
			(finish_mission, 0),
			(jump_to_menu, "mnu_camp"),
		]),
		
		spawn_trigger_town, 
    ] + common_triggers),
	
	("mmo_adventure", 0, -1,
    "Adventure",
    [
        (0, mtef_scene_source|mtef_team_0, af_override_horse, 0, 1, []),
    ],
    [
		(ti_tab_pressed, 0, 0, [], [
			(finish_mission, 0),
			(jump_to_menu, "mnu_camp"),
		]),
		spawn_trigger_adventure, debug_cheat_triggers,
    ] + common_triggers),
	
	("mmo_raid", 0, -1,
    "Raid",
    [
        (0, mtef_scene_source|mtef_team_0, af_override_horse, 0, 1, []),
    ],
    [
		(ti_tab_pressed, 0, 0, [], [
			(finish_mission, 0),
			(jump_to_menu, "mnu_camp"),
		]),
	
    ] + common_triggers),
]
