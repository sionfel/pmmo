from header_common import *
from header_presentations import *
from header_mission_templates import *
from ID_meshes import *
from header_operations import *
from header_triggers import *
from module_constants import *
import string

# #######################################################################
#
#	("presentation_id", flags, bg_mesh, [triggers]),
#
#  Each presentation record contains the following fields:
#  1) Presentation id: used for referencing presentations in other files. The prefix prsnt_ is automatically added before each presentation id.
#  2) Presentation flags. See header_presentations.py for a list of available flags
#  3) Presentation background mesh: See module_meshes.py for a list of available background meshes
#  4) Triggers: Simple triggers that are associated with the presentation
# #######################################################################
presentations = [

    # #######################################################################
    # 		Hardcoded, mostly things the engine will try to call and will
	#		cause errors if not declared, even if they do nothing.
	# #######################################################################
	
	# #######################################################################
	# 	Called by engine when game starts, can be used to add to the main
	# 	menu. The funny thing is that if it doesn't exist, there is no error,
	#	but if it does exist it ~A B S O L U T E L Y~ must be first.
	# #######################################################################
	("game_start", 0, 0,
		[(ti_on_presentation_load,[(presentation_set_duration, 0)]),
	]),
	
	# Called by engine when pressing `ESC`, can be used to add to the escape menu
	("game_escape", 0, 0,
		[(ti_on_presentation_load,[(presentation_set_duration, 0)]),
	]),
	
	# Called by pressing the credits button on the main menu
	("game_credits", 0, 0,
		[(ti_on_presentation_load,[(presentation_set_duration, 0)]),
	]),
	
	# #######################################################################
	# Called by engine when setting up a banner for Multiplayer, the engine
	# looks for its existance even if your module.ini has_multiplayer = 0
	# #######################################################################
	("game_profile_banner_selection", 0, 0,
		[(ti_on_presentation_load,[(presentation_set_duration, 0)]),
	]),
	
	# #######################################################################
	# Called by engine when setting up a custom battle from the main menu.
	#	Copy and Paste from Native if you would like to re-add,
	#	If you want to take out entirely, find :
	# initial_custom_battle_button_size_x, initial_custom_battle_button_size_y
	# initial_custom_battle_button_text_size_x, initial_custom_battle_button_text_size_y
	#	in game_variables.txt and set to 0.0
	# #######################################################################
	("game_custom_battle_designer", 0, 0,
		[(ti_on_presentation_load,[(presentation_set_duration, 0)]),
	]),
	
	# #######################################################################
	# Called by engine when looking at the admin menu in multiplayer, 
	# is hardcoded an required even if your module.ini has_multiplayer = 0
	# #######################################################################
	("game_multiplayer_admin_panel", 0, 0,
		[(ti_on_presentation_load,[(presentation_set_duration, 0)]),
	]),
	
	# #######################################################################
	# Called by the engine when a player exits the game. Basically used
	# in Native to show a purchase promo screen in demo mode.
	# #######################################################################
	("game_before_quit", 0, 0,
		[(ti_on_presentation_load,[(presentation_set_duration, 0)]),
	]),
	# #######################################################################
	#		Add your stuff here. 
	#			Good Luck! You'll need it!
    # #######################################################################
	
	("display_agent_labels", prsntf_read_only|prsntf_manual_end_only, 0, # display player name and optionally faction name above the heads of nearby agents
	[
		(ti_on_presentation_load,
		[
			(assign, "$g_presentation_agent_labels_overlay_count", 0),
			(assign, "$g_presentation_agent_labels_update_time", 0),
			(presentation_set_duration, 999999),
		]),
		  
		(ti_on_presentation_run,
		[
			(store_trigger_param_1, ":current_time"),
			(set_fixed_point_multiplier, 1000),
			(try_begin),
			# 	(eq, "$g_display_agent_labels", 0),
			# 	(presentation_set_duration, 0),
			# (else_try),
				(gt, ":current_time", "$g_presentation_agent_labels_update_time"),
				(store_add, "$g_presentation_agent_labels_update_time", ":current_time", 20), # check and update all visible labels every 20 milliseconds
				# (multiplayer_get_my_player, ":my_player"),
				# (gt, ":my_player", -1),
				(str_clear, s0),
				(mission_cam_get_position, pos1),
				(assign, ":overlay_id", 0),			
				
				(try_for_agents, ":agent_id", pos1, 15000),
					(agent_is_human, ":agent_id"),
					(agent_is_alive, ":agent_id"),
					(agent_is_non_player, ":agent_id"),
					# (agent_get_player_id, ":player_id", ":agent_id"),
					# (neq, ":player_id", ":my_player"),
					# (agent_get_item_slot, ":body_item_id", ":agent_id", ek_body),
					# (neq, ":body_item_id", "itm_invisible_body"),
					(agent_get_position, pos2, ":agent_id"),
					(position_move_z, pos2, 150),
					(agent_get_horse, ":horse", ":agent_id"),
					
					(try_begin),
						(ge, ":horse", 0),
						(position_move_z, pos2, 80),
					(try_end),
					
					(get_distance_between_positions_in_meters, ":distance", pos1, pos2),
					# (le, ":sq_distance", 15000),
					(copy_position, pos3, pos2),
						(try_begin),
							(eq, "$g_basic_name_labels", 1),
							(position_move_z, pos3, 30),
						(else_try),
							(position_move_z, pos3, 50),
						(try_end),
					(position_get_screen_projection, pos4, pos3), # get the 2D position on screen 50 above the 3D position of the agent
					(position_get_x, ":x_pos", pos4),
					(position_get_y, ":y_pos", pos4),
					(is_between, ":x_pos", -100, 1100), # check if inside the view boundaries or nearly so
					(is_between, ":y_pos", -100, 850),
					(position_has_line_of_sight_to_position, pos1, pos2),
					
					(try_begin), # create a new overlay if needed
						(ge, ":overlay_id", "$g_presentation_agent_labels_overlay_count"),
						(create_text_overlay, reg0, s0, tf_center_justify|tf_with_outline),
						(val_add, "$g_presentation_agent_labels_overlay_count", 1),
					(try_end),
					
					(try_begin),
						#(try_begin),
							# (eq, "$g_basic_name_labels", 0),
							# (try_begin),
							# 	(player_is_active, ":player_id"),
							# 	(str_store_player_username, s2, ":player_id"),
							# 	(player_get_slot, ":player_faction_id", ":player_id", slot_player_faction_id),
							# 	(try_begin),
							# 		(eq, "$g_hide_faction_in_name_labels", 0),
							# 		(str_store_faction_name, s3, ":player_faction_id"),
							# 		(str_store_string, s2, "str_s2_s3"),
							# 	(try_end),
							# 	(faction_get_color, ":color", ":player_faction_id"),
							# (else_try),
								(agent_get_slot, ":activity", ":agent_id", ai_agent_activity),
								
								(try_begin),
									(eq, ":activity", activity_wandering),
									(str_store_string, s13, "@Wandering"),
								(else_try),
									(eq, ":activity", activity_fighting),
									(str_store_string, s13, "@Fighting"),
								(else_try),
									(eq, ":activity", activity_hunting),
									(str_store_string, s13, "@Hunting"),
								(else_try),
									(eq, ":activity", activity_woodcutting),
									(str_store_string, s13, "@WC"),
								(else_try),
									(eq, ":activity", activity_fishing),
									(str_store_string, s13, "@Fishing"),
								(else_try),
									(eq, ":activity", activity_farming),
									(str_store_string, s13, "@Farming"),
								(else_try),
									(eq, ":activity", activity_patrolling),
									(str_store_string, s13, "@Patrolling"),
								(else_try),
									(eq, ":activity", activity_guarding),
									(str_store_string, s13, "@Guarding"),
								(else_try),
									(eq, ":activity", activity_vendor),
									(str_store_string, s13, "@Selling"),
								(try_end),
								
								(agent_get_troop_id, ":troop_id", ":agent_id"),
								(store_character_level, reg2, ":troop_id"),
								(str_store_troop_name, s2, ":troop_id"),
								(troop_get_slot, ":p", ":troop_id", troop_party_id),
								(str_store_party_name, s11, ":p"),
								(str_store_string, s2, "@Lv{reg2}\t{s2}^{s11}^{s13}"),
							#	(assign, ":color", invalid_faction_color),
						# 	(try_end),
						# (else_try),
						# 	(try_begin),
						# 		(player_is_active, ":player_id"),
						# 		(str_store_string, s2, "str_basic_name_labels"),
						# 		(player_get_slot, ":player_faction_id", ":player_id", slot_player_faction_id),
						# 		(faction_get_color, ":color", ":player_faction_id"),
						# 	(else_try),
						# 		(str_store_string, s2, "str_basic_name_labels"),
						# 		(assign, ":color", invalid_faction_color),
						# 	(try_end),
						#(try_end),
						
						(overlay_set_text, ":overlay_id", s2),
						(overlay_set_color, ":overlay_id", 0xffffff),
						(overlay_set_position, ":overlay_id", pos4),
						(try_begin),
							(le, ":distance", 1),
							(assign, ":height", 1000),
						(else_try),
							(copy_position, pos5, pos3),
							(position_move_z, pos5, 50), # move the 3D position up by 200 and check the 2D distance moved, to get the appropriate text size (reducing with distance)
							(position_get_screen_projection, pos6, pos5),
							(position_get_y, ":y_pos_up", pos6),
							(store_sub, ":height", ":y_pos_up", ":y_pos"),
							(val_mul, ":height", 10),
							(val_min, ":height", 1000),
						(try_end),
						(position_set_y, pos6, ":height"),
						(position_set_x, pos6, ":height"),
						(overlay_set_size, ":overlay_id", pos6),
						(overlay_set_display, ":overlay_id", 1),
					(try_end),
					(val_add, ":overlay_id", 1),
			(try_end),
			
			(try_begin),
				(store_sub, ":redraw_threshold", "$g_presentation_agent_labels_overlay_count", 10),
				(lt, ":overlay_id", ":redraw_threshold"),
				(presentation_set_duration, 0),
				(start_presentation, "prsnt_display_agent_labels"),
			(else_try),
				(try_for_range, ":unused_overlay_id", ":overlay_id", "$g_presentation_agent_labels_overlay_count"),
					(overlay_set_display, ":unused_overlay_id", 0),
				(try_end),
			(try_end),
			(try_end),
		  ]),
    ]),
]
