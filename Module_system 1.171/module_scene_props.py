from header_common import *
from header_scene_props import *
from header_operations import *
from header_triggers import *
from header_sounds import *
from module_constants import *
import string


# #######################################################################
# ("prop_id", flags, "mesh_name", "collider_name", [triggers]),
#
#  Each scene prop record contains the following fields:
#  1) Scene prop id: used for referencing scene props in other files. The prefix spr_ is automatically added before each scene prop id.
#  2) Scene prop flags. See header_scene_props.py for a list of available flags
#  3) Mesh name: Name of the mesh.
#  4) Physics object name: "bo_"
#  5) Triggers: Simple triggers that are associated with the scene prop
# #######################################################################

scene_props = [
  ("invalid_object",0,"question_mark","0", []),
  ("inventory",sokf_type_container|sokf_place_at_origin,"package","bobaggage", []),
  ("empty", 0, "0", "0", []),
  ("chest_a",sokf_type_container,"chest_gothic","bochest_gothic", []),
  ("container_small_chest",sokf_type_container,"package","bobaggage", []),
  ("container_chest_b",sokf_type_container,"chest_b","bo_chest_b", []),
  ("container_chest_c",sokf_type_container,"chest_c","bo_chest_c", []),
  ("player_chest",sokf_type_container,"player_chest","bo_player_chest", []),
  ("locked_player_chest",0,"player_chest","bo_player_chest", []),

  ("light_sun",sokf_invisible,"light_sphere","0",  [
     (ti_on_init_scene_prop,
      [
          (neg|is_currently_night),
          (store_trigger_param_1, ":prop_instance_no"),
          (set_fixed_point_multiplier, 100),
          (prop_instance_get_scale, pos5, ":prop_instance_no"),
          (position_get_scale_x, ":scale", pos5),
          (store_time_of_day,reg(12)),
          (try_begin),
            (is_between,reg(12),5,20),
            (store_mul, ":red", 5 * 200, ":scale"),
            (store_mul, ":green", 5 * 193, ":scale"),
            (store_mul, ":blue", 5 * 180, ":scale"),
          (else_try),
            (store_mul, ":red", 5 * 90, ":scale"),
            (store_mul, ":green", 5 * 115, ":scale"),
            (store_mul, ":blue", 5 * 150, ":scale"),
          (try_end),
          (val_div, ":red", 100),
          (val_div, ":green", 100),
          (val_div, ":blue", 100),
          (set_current_color,":red", ":green", ":blue"),
          (set_position_delta,0,0,0),
          (add_point_light_to_entity, 0, 0),
      ]),
    ]),
	
  ("light",sokf_invisible,"light_sphere","0",  [
     (ti_on_init_scene_prop,
      [
          (store_trigger_param_1, ":prop_instance_no"),
          (set_fixed_point_multiplier, 100),
          (prop_instance_get_scale, pos5, ":prop_instance_no"),
          (position_get_scale_x, ":scale", pos5),
          (store_mul, ":red", 3 * 200, ":scale"),
          (store_mul, ":green", 3 * 145, ":scale"),
          (store_mul, ":blue", 3 * 45, ":scale"),
          (val_div, ":red", 100),
          (val_div, ":green", 100),
          (val_div, ":blue", 100),
          (set_current_color,":red", ":green", ":blue"),
          (set_position_delta,0,0,0),
          (add_point_light_to_entity, 10, 30),
      ]),
    ]),

  ("light_night",sokf_invisible,"light_sphere","0",  [
     (ti_on_init_scene_prop,
      [
#          (store_time_of_day,reg(12)),
#          (neg|is_between,reg(12),5,20),
          (is_currently_night, 0),
          (store_trigger_param_1, ":prop_instance_no"),
          (set_fixed_point_multiplier, 100),
          (prop_instance_get_scale, pos5, ":prop_instance_no"),
          (position_get_scale_x, ":scale", pos5),
          (store_mul, ":red", 3 * 160, ":scale"),
          (store_mul, ":green", 3 * 145, ":scale"),
          (store_mul, ":blue", 3 * 100, ":scale"),
          (val_div, ":red", 100),
          (val_div, ":green", 100),
          (val_div, ":blue", 100),
          (set_current_color,":red", ":green", ":blue"),
          (set_position_delta,0,0,0),
          (add_point_light_to_entity, 10, 30),
      ]),
    ]),
	
  ("barrier_20m",sokf_invisible|sokf_type_barrier,"barrier_20m","bo_barrier_20m", []),
  ("barrier_16m",sokf_invisible|sokf_type_barrier,"barrier_16m","bo_barrier_16m", []),
  ("barrier_8m" ,sokf_invisible|sokf_type_barrier,"barrier_8m" ,"bo_barrier_8m" , []),
  ("barrier_4m" ,sokf_invisible|sokf_type_barrier,"barrier_4m" ,"bo_barrier_4m" , []),
  ("barrier_2m" ,sokf_invisible|sokf_type_barrier,"barrier_2m" ,"bo_barrier_2m" , []),
  ("barrier_box",sokf_invisible|sokf_type_barrier3d,"barrier_box","bo_barrier_box", []),
  ("barrier_capsule",sokf_invisible|sokf_type_barrier3d,"barrier_capsule","bo_barrier_capsule", []),
  ("barrier_cone" ,sokf_invisible|sokf_type_barrier3d,"barrier_cone" ,"bo_barrier_cone" , []),
  ("barrier_sphere" ,sokf_invisible|sokf_type_barrier3d,"barrier_sphere" ,"bo_barrier_sphere" , []),
  
  ("exit_4m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_4m" ,"bo_barrier_4m" , []),
  ("exit_8m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_8m" ,"bo_barrier_8m" , []),
  ("exit_16m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_16m" ,"bo_barrier_16m" , []),

  ("ai_limiter_2m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_2m" ,"bo_barrier_2m" , []),
  ("ai_limiter_4m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_4m" ,"bo_barrier_4m" , []),
  ("ai_limiter_8m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_8m" ,"bo_barrier_8m" , []),
  ("ai_limiter_16m",sokf_invisible|sokf_type_ai_limiter,"barrier_16m","bo_barrier_16m", []),

  ("arena_tower_c",0,"arena_tower_c","bo_arena_tower_abc", []),
  ("crude_fence",0,"fence","bo_fence", []),
  ("crude_fence_small",0,"crude_fence_small","bo_crude_fence_small", []),
  ("crude_fence_small_b",0,"crude_fence_small_b","bo_crude_fence_small_b", []),
  ("end_table_a",0,"end_table_a","bo_end_table_a", []),
  ("full_keep_b",0,"full_keep_b","bo_full_keep_b", []),
  ("full_stable_a",0,"full_stable_a","bo_full_stable_a", []),
  ("gate_house_a",0,"gate_house_a","bo_gate_house_a", []),
  ("grave_a",0,"grave_a","bo_grave_a", []),
  ("merchant_sign",0,"merchant_sign","bo_tavern_sign", []),
  ("mine_a",0,"mine_a","bo_mine_a", []),
  ("obstacle_fallen_tree_a",0,"destroy_tree_a","bo_destroy_tree_a", []),
  ("obstacle_fallen_tree_b",0,"destroy_tree_b","bo_destroy_tree_b", []),
  ("passage_house_a",0,"passage_house_a","bo_passage_house_a", []),
  ("railing_a",0,"railing_a","bo_railing_a", []),
  ("ship_sail_off",0,"ship_sail_off","bo_ship_sail_off", []),
  ("skeleton_cow",0,"skeleton_cow","0", []),
  ("skeleton_head",0,"skeleton_head","0", []),
  ("small_round_tower_a",0,"small_round_tower_a","bo_small_round_tower_a", []),
  ("smithy_anvil", 0,"smithy_anvil","bo_smithy_anvil", []),
  ("smithy_forge", 0,"smithy_forge","bo_smithy_forge", []),
  ("smithy_forge_bellows", 0,"smithy_forge_bellows","bo_smithy_forge_bellows", []),
  ("smithy_grindstone_wheel", 0,"smithy_grindstone_wheel","bo_smithy_grindstone_wheel", []),
  ("spike_a",0,"spike_a","bo_spike_a", []),
  ("spike_group_a",0,"spike_group_a","bo_spike_group_a", []),
  ("stand_cloth",0,"stand_cloth","bo_stand_cloth", []),
  ("stand_thatched",0,"stand_thatched","bo_stand_thatched", []),
  ("table_small",0,"table_small","bo_table_small", []),
  ("table_trunk_a",0,"table_trunk_a","bo_table_trunk_a", []),
  ("timber_frame_extension_b",0,"timber_frame_extension_b","bo_timber_frame_extension_b", []),
  ("torture_tool_a",0,"torture_tool_a","bo_torture_tool_a", []),
  ("water_river",0,"water_plane","0", []),
  ("water_well_a",0,"water_well_a","bo_water_well_a", []),
  ("weapon_rack",0,"weapon_rack","boweapon_rack", []),
  ("weapon_rack_big",0,"weapon_rack_big","boweapon_rack_big", []),
  ("wheelbarrow",0,"wheelbarrow","bo_wheelbarrow", []),
  ("windmill",0,"windmill","bo_windmill", []),
  ("windmill_fan",0,"windmill_fan","bo_windmill_fan", []),
  ("windmill_fan_turning",sokf_moveable,"windmill_fan_turning","bo_windmill_fan_turning", []),
  ("winery_barrel_shelf",0,"winery_barrel_shelf","bo_winery_barrel_shelf", []),
  ("wood_heap",0,"wood_heap_a","bo_wood_heap_a", []),
  ("wood_heap_b",0,"wood_heap_b","bo_wood_heap_b", []),
  
  ("small_round_tower_stacker",0,"small_round_tower_a","bo_small_round_tower_a", [
	(ti_on_init_scene_prop, [
		(set_fixed_point_multiplier, 100),
		(store_trigger_param_1, ":prop_instance_no"),
		(prop_instance_get_position, pos10, ":prop_instance_no"),
		
		(try_for_range, ":i", 1, 30),
			(copy_position, pos11, pos10),
			(store_mul, ":z_offset", 1200, ":i"),
			(position_move_z, pos11, ":z_offset"),
			(set_spawn_position, pos11),
			(spawn_scene_prop, "spr_small_round_tower_a"),
		(try_end),
	])
  ]),
  

  ("player_spawner", sokf_invisible, "question_mark", "0", []),
  ("point_of_interest", sokf_invisible, "question_mark", "0", []),
  ("gathering_spot", sokf_invisible, "question_mark", "0", []),
  ("teleporter", sokf_invisible, "question_mark", "0", []),
  ("scene_entrance", sokf_invisible, "question_mark", "0", []),
  
  ("merchant_spawner", sokf_invisible, "question_mark", "0", [ ]),
  
  ("enemy_spawner", sokf_invisible, "question_mark", "0", []),

]

