from header_common import *
from header_operations import *
from module_constants import *
from module_constants import *
from header_parties import *
from header_skills import *
from header_mission_templates import *
from header_items import *
from header_triggers import *
from header_terrain_types import *
from header_music import *
from ID_animations import *

# #######################################################################
# scripts is a list of script records.
# Each script record contains the following two fields:
# 1) Script id: The prefix "script_" will be inserted when referencing scripts.
# 2) Operation block: This must be a valid operation block. See header_operations.py for reference.
# #######################################################################

# #######################################################################
#		(script_id, [operation_block]),
#	Pretty simple stuff. It's also pretty standard practice for the person making the code to do something like
#			#script_script_id:
# 			#This is a description of the script
# 			#INPUT:
#			#	Param 1: 
#			#	Param 2:
#			#OUTPUT: 
#			#	What the output would be.
# #######################################################################

# #######################################################################
#		I've left a few things intact that aren't strictly harcoded,
#		mostly because I don't have the time to build a mod to test to
#		see what they may break. And by that I mean tableau scripts.
# #######################################################################

scripts = [	
	
	# #######################################################################
	# 	Engine Scripts, the engine will look for these on load 
	#	so they you need them to prevent errors but they are not required 
	#	to stay in this order or to actually function.
	# #######################################################################

	#script_game_start:
	# This script is called by the engine when a new game is started
	# INPUT: none
	# OUTPUT: none
	("game_start",
	[
		(set_show_messages, 0),
		(store_add, ":parties_end", player_parties_begin, 300),
		
	
		(try_for_range, ":player", players_begin, mobs_begin),
			# (str_store_troop_name, s10, ":player"),
		
			(assign, ":loop_kill", 0),
			(try_for_range, ":i", 0, 250),
				(neq, ":loop_kill", 1),
				(store_random_in_range, ":p", player_parties_begin, ":parties_end"),
				(party_get_num_companions, ":count", ":p"),
				
				
				(le, ":count", 3),
				(assign, ":loop_kill", 1),
			(try_end),
			
			(party_add_members, ":p", ":player", 1),
			(troop_set_slot, ":player", troop_party_id, ":p"),
			# (str_store_party_name, s11, ":p"),
			
			# (display_log_message, "@Hi, I am {s10} and I belong to {s11}"),
			
		(try_end),
		
		(try_for_range, ":mob", mobs_begin, "trp_end_troops"),
		
			(store_character_level, ":level", ":mob"),
			(val_div, ":level", 3),
			(val_max, ":level", 3),
			(troop_set_slot, ":mob", troop_supports, ":level"),
			
		(try_end),
		
		(try_for_range, ":p", 0, ":parties_end"),
			(assign, reg1, ":p"),
			(str_store_party_name, s11, ":p"),
			(display_log_message, "@Party ID: {reg1}, {s11}"),
			(party_get_num_companions, ":count", ":p"),
			(try_for_range, ":member", 0, ":count"),
				(assign, reg1, ":member"),
				(party_stack_get_troop_id, ":t", ":p", ":member"),
				(str_store_troop_name, s10, ":t"),
				(display_log_message, "@\t\t\t{reg1}: {s10}"),
			(try_end),
		(try_end),
		
		(set_show_messages, 1),
	
	]),

	#script_game_quick_start
	# This script is called from the game engine for initializing the global variables 
	# for tutorial, multiplayer and custom battle modes. Similar to game_start without 
	# the stuff for map and diplomacy things
	# INPUT: none
	# OUTPUT: none
	("game_quick_start",
	[
	]),

	#script_game_set_multiplayer_mission_end
	# This script is called from the game engine when a multiplayer map is ended in clients (not in server).
	# INPUT: none
	# OUTPUT: none
	("game_set_multiplayer_mission_end",
	[
		(assign, "$g_multiplayer_mission_end_screen", 1),
	]),
	
	#script_game_get_use_string
	# This script is called from the game engine for getting using information text
	# INPUT: used_scene_prop_id
	# OUTPUT: s0
	("game_get_use_string",
	[
	]),
	
	#script_game_enable_cheat_menu
	# This script is called from the game engine when user enters "cheatmenu from command console (ctrl+~).
	# INPUT:
	# none
	# OUTPUT:
	# none
	("game_enable_cheat_menu",
	[
	]),

	# script_game_event_party_encounter:
	# This script is called from the game engine whenever player party encounters another party or a battle on the world map
	# INPUT:
	# param1: encountered_party
	# param2: second encountered_party (if this was a battle
	("game_event_party_encounter",
	[
		# This script is left intact as I don't have parties to test what would happen if it were empty
		(store_script_param_1, "$g_encountered_party"),
		(store_script_param_2, "$g_encountered_party_2"), # Negative if non-battle

		(try_begin),
			(lt, "$g_encountered_party_2", 0), # non-battle
			(try_begin),
				(eq, "$g_encountered_party", "p_camp_bandits"), # camp
				(jump_to_menu, "mnu_camp"),
			(try_end),
		(try_end),
	]),

	#script_game_event_simulate_battle:
	# This script is called whenever the game simulates the battle between two parties on the map.
	# INPUT:
	# param1: Defender Party
	# param2: Attacker Party
	("game_event_simulate_battle",
	[
	]),

	#script_game_event_battle_end:
	# This script is called whenever the game ends the battle between two parties on the map.
	# INPUT:
	# param1: Defender Party
	# param2: Attacker Party
	("game_event_battle_end",
	[
	]), 

	#script_game_get_item_buy_price_factor:
	# This script is called from the game engine for calculating the buying price of any item.
	# INPUT:
	# param1: item_kind_id
	# OUTPUT:
	# trigger_result and reg0 = price_factor
	("game_get_item_buy_price_factor",
	[
	]),

	#script_game_get_item_sell_price_factor:
	# This script is called from the game engine for calculating the selling price of any item.
	# INPUT:
	# param1: item_kind_id
	# OUTPUT:
	# trigger_result and reg0 = price_factor
	("game_get_item_sell_price_factor",
	[
	]),

	#script_game_event_buy_item:
	# This script is called from the game engine when player buys an item.
	# INPUT:
	# param1: item_kind_id
	("game_event_buy_item",
	[
	]),

	#script_game_event_sell_item:
	# This script is called from the game engine when player sells an item.
	# INPUT:
	# param1: item_kind_id
	("game_event_sell_item",
	[
	]),	

	# script_game_get_troop_wage
	# This script is called from the game engine for calculating troop wages.
	# Input:
	# param1: troop_id, param2: party-id
	# Output: reg0: weekly wage
	("game_get_troop_wage",
	[
	]),

	# script_game_get_total_wage
	# This script is called from the game engine for calculating total wage of the player party which is shown at the party window.
	# Input: none
	# Output: reg0: weekly wage
	("game_get_total_wage",
	[
	]),

	# script_game_get_join_cost
	# This script is called from the game engine for calculating troop join cost.
	# Input:
	# param1: troop_id,
	# Output: reg0: weekly wage
	("game_get_join_cost",
	[
	]),

	# script_game_get_upgrade_xp
	# This script is called from game engine for calculating needed troop upgrade exp
	# Input:
	# param1: troop_id,
	# Output: reg0 = needed exp for upgrade 
	("game_get_upgrade_xp",
	[
	]),

	# script_game_get_upgrade_cost
	# This script is called from game engine for calculating needed troop upgrade exp
	# Input:
	# param1: troop_id,
	# Output: reg0 = needed cost for upgrade
	("game_get_upgrade_cost",
	[
	]),

	# script_game_get_prisoner_price
	# This script is called from the game engine for calculating prisoner price
	# Input:
	# param1: troop_id,
	# Output: reg0
	("game_get_prisoner_price",
	[
	]),


	# script_game_check_prisoner_can_be_sold
	# This script is called from the game engine for checking if a given troop can be sold.
	# Input: 
	# param1: troop_id,
	# Output: reg0: 1= can be sold; 0= cannot be sold.
	("game_check_prisoner_can_be_sold",
	[
	]),

	# script_game_get_morale_of_troops_from_faction
	# This script is called from the game engine 
	# Input: 
	# param1: faction_no,
	# Output: reg0: extra morale x 100
	("game_get_morale_of_troops_from_faction",
	[
	]),

	#script_game_event_detect_party:
	# This script is called from the game engine when player party inspects another party.
	# INPUT:
	# param1: Party-id
	("game_event_detect_party",
	[
	]),

	#script_game_event_undetect_party:
	# This script is called from the game engine when player party inspects another party.
	# INPUT:
	# param1: Party-id
	("game_event_undetect_party",
	[
	]),

	#script_game_get_statistics_line:
	# This script is called from the game engine when statistics page is opened.
	# INPUT:
	# param1: line_no
	("game_get_statistics_line",
	[
	]),

	#script_game_get_date_text:
	# This script is called from the game engine when the date needs to be displayed.
	# INPUT: arg1 = number of days passed since the beginning of the game
	# OUTPUT: result string = date
	("game_get_date_text",
	[
	]),

	#script_game_get_money_text:
	# This script is called from the game engine when an amount of money needs to be displayed.
	# INPUT: arg1 = amount in units
	# OUTPUT: result string = money in text
	("game_get_money_text",
	[
	]),

	#script_game_get_party_companion_limit:
	# This script is called from the game engine when the companion limit is needed for a party.
	# INPUT: arg1 = none
	# OUTPUT: reg0 = companion_limit
	("game_get_party_companion_limit",
	[
	]),


	#script_game_reset_player_party_name:
	# This script is called from the game engine when the player name is changed.
	# INPUT: none
	# OUTPUT: none
	("game_reset_player_party_name",
	[
	]),

	#script_game_get_troop_note
	# This script is called from the game engine when the notes of a troop is needed.
	# INPUT: arg1 = troop_no, arg2 = note_index
	# OUTPUT: s0 = note
	("game_get_troop_note",
	[
	]),

	#script_game_get_center_note
	# This script is called from the game engine when the notes of a center is needed.
	# INPUT: arg1 = center_no, arg2 = note_index
	# OUTPUT: s0 = note
	("game_get_center_note",
	[
	]),

	#script_game_get_faction_note
	# This script is called from the game engine when the notes of a faction is needed.
	# INPUT: arg1 = faction_no, arg2 = note_index
	# OUTPUT: s0 = note
	("game_get_faction_note",
	[
	]),

	#script_game_get_quest_note
	# This script is called from the game engine when the notes of a quest is needed.
	# INPUT: arg1 = quest_no, arg2 = note_index
	# OUTPUT: s0 = note
	("game_get_quest_note",
	[
	]),

	#script_game_get_info_page_note
	# This script is called from the game engine when the notes of a info_page is needed.
	# INPUT: arg1 = info_page_no, arg2 = note_index
	# OUTPUT: s0 = note
	("game_get_info_page_note",
	[
	]),

	#script_game_get_scene_name
	# This script is called from the game engine when a name for the scene is needed.
	# INPUT: arg1 = scene_no
	# OUTPUT: s0 = name
	("game_get_scene_name",
	[
	]),

	#script_game_get_mission_template_name
	# This script is called from the game engine when a name for the mission template is needed.
	# INPUT: arg1 = mission_template_no
	# OUTPUT: s0 = name
	("game_get_mission_template_name",
	[
	]),

	#script_game_receive_url_response
	# response format should be like this:
	# [a number or a string]|[another number or a string]|[yet another number or a string] ...
	# here is an example response:
	# 12|Player|100|another string|142|323542|34454|yet another string
	# INPUT: arg1 = num_integers, arg2 = num_strings
	# reg0, reg1, reg2, ... up to 128 registers contain the integer values
	# s0, s1, s2, ... up to 128 strings contain the string values
	("game_receive_url_response",
	[
	]),
	#script_game_get_cheat_mode
	# dstn speculation for this whole entry: 
	# Assuming this script determines whether or not cheat mode on the ctrl+~ 
	# command line has been activated.
	# INPUT: NONE
	# OUTPUT: reg0 = cheatmenu_status, 0 for inactive, 1 for active. I assume. 
	("game_get_cheat_mode",
	[
	]),

	#script_game_receive_network_message
	# This script is called from the game engine when a new network message is received.
	# INPUT: arg1 = player_no, arg2 = event_type, arg3 = value, arg4 = value_2, arg5 = value_3, arg6 = value_4
	("game_receive_network_message",
	[
	]),

	#script_game_get_multiplayer_server_option_for_mission_template
	# Input: arg1 = mission_template_id, arg2 = option_index
	# Output: trigger_result = 1 for option available, 0 for not available
	# reg0 = option_value
	("game_get_multiplayer_server_option_for_mission_template",
	[
	]),

	#script_game_multiplayer_server_option_for_mission_template_to_string
	# Input: arg1 = mission_template_id, arg2 = option_index, arg3 = option_value
	# Output: s0 = option_text
	("game_multiplayer_server_option_for_mission_template_to_string",
	[
	]),

	#script_game_multiplayer_event_duel_offered
	# Input: arg1 = agent_no
	# Output: none
	("game_multiplayer_event_duel_offered",
	[
	]),
		 
	#script_game_get_multiplayer_game_type_enum
	# Input: none
	# Output: reg0:first type, reg1:type count
	("game_get_multiplayer_game_type_enum",
	[
	]),

	#script_game_multiplayer_get_game_type_mission_template
	# Input: arg1 = game_type
	# Output: mission_template 
	("game_multiplayer_get_game_type_mission_template",
	[
	]),

	#script_game_get_party_prisoner_limit
	# This script is called from the game engine when the prisoner limit is needed for a party.
	# INPUT: arg1 = party_no
	# OUTPUT: reg0 = prisoner_limit
	("game_get_party_prisoner_limit",
	[
	]),

	#script_game_get_item_extra_text:
	# This script is called from the game engine when an item's properties are displayed.
	# INPUT: arg1 = item_no, arg2 = extra_text_id (this can be between 0-7 (7 included)), arg3 = item_modifier
	# OUTPUT: result_string = item extra text, trigger_result = text color (0 for default)
	("game_get_item_extra_text",
	[
	]),

	#script_game_on_disembark:
	# This script is called from the game engine when the player reaches the shore with a ship.
	# INPUT: pos0 = disembark position
	# OUTPUT: none
	("game_on_disembark",
	[
	]),


	#script_game_context_menu_get_buttons:
	# This script is called from the game engine when the player clicks the right mouse button over a party on the map.
	# INPUT: arg1 = party_no
	# OUTPUT: none, fills the menu buttons
	("game_context_menu_get_buttons",
	[
	]),

	#script_game_event_context_menu_button_clicked:
	# This script is called from the game engine when the player clicks on a button at the right mouse menu.
	# INPUT: arg1 = party_no, arg2 = button_value
	# OUTPUT: none
	("game_event_context_menu_button_clicked",
	[
	]),

	#script_game_get_skill_modifier_for_troop
	# This script is called from the game engine when a skill's modifiers are needed
	# INPUT: arg1 = troop_no, arg2 = skill_no
	# OUTPUT: trigger_result = modifier_value
	("game_get_skill_modifier_for_troop",
	[
	]),

	#script_game_check_party_sees_party
	# This script is called from the game engine when a party is inside the range of another party
	# INPUT: arg1 = party_no_seer, arg2 = party_no_seen
	# OUTPUT: trigger_result = true or false (1 = true, 0 = false)
	("game_check_party_sees_party",
	[
		(set_trigger_result, 1),
	]),

	#script_game_get_party_speed_multiplier
	# This script is called from the game engine when a skill's modifiers are needed
	# INPUT: arg1 = party_no
	# OUTPUT: trigger_result = multiplier (scaled by 100, meaning that giving 100 as the trigger result does not change the party speed)
	("game_get_party_speed_multiplier",
	[
		(set_trigger_result, 100),
	]),

	#script_game_get_console_command
	# This script is called from the game engine when a console command is entered from the dedicated server.
	# INPUT: anything
	# OUTPUT: s0 = result text
	("game_get_console_command",
	[
	]),
	
	# script_game_missile_launch
	# Input: arg1 = shooter_agent_id, arg2 = agent_weapon_item_id, 
	# arg3 = missile_weapon_id, arg4 = missile_item_id
	# pos1 = weapon_item_position
	# Output: none 
	("game_missile_launch",
	[
	]),
	
	# script_game_missile_dives_into_water
	# Input: arg1 = missile_item_id, pos1 = missile_position_on_water
	# Output: none
	("game_missile_dives_into_water",
	[
	]),
	
	#script_game_troop_upgrades_button_clicked
	# This script is called from the game engine when the player clicks on said button from the party screen
	# INPUT: arg1 = troop_id
	("game_troop_upgrades_button_clicked",
	[
	]),
	
	#script_game_troop_upgrades_button_clicked
	# This script is called from the game engine when the player clicks the character button or presses the
	# relevant gamekey default is 'c'
	("game_character_screen_requested",
	[
	]),
	
# #######################################################################
# 	Tableau Scripts! I could probably  have removed these outright, 
# 	but I think I'll leave them in.	Mostly because I know nothing 
# 	about tableau and I don't feel confident that I could erase the 
# 	few left in the code. I did remove the retirement tableau!				
# #######################################################################
	
	#script_add_troop_to_cur_tableau
	# INPUT: troop_no
	# OUTPUT: none
	("add_troop_to_cur_tableau",
	[
		(store_script_param, ":troop_no",1),

		(set_fixed_point_multiplier, 100),
		(assign, ":banner_mesh", -1),

		(cur_tableau_clear_override_items),
		 
		(cur_tableau_set_override_flags, af_override_head|af_override_weapons),
		 
		(init_position, pos2),
		(cur_tableau_set_camera_parameters, 1, 6, 6, 10, 10000),

		(init_position, pos5),
		(assign, ":eye_height", 162),
		(store_mul, ":camera_distance", ":troop_no", 87323),
		(assign, ":camera_distance", 139),
		(store_mul, ":camera_yaw", ":troop_no", 124337),
		(val_mod, ":camera_yaw", 50),
		(val_add, ":camera_yaw", -25),
		(store_mul, ":camera_pitch", ":troop_no", 98123),
		(val_mod, ":camera_pitch", 20),
		(val_add, ":camera_pitch", -14),
		(assign, ":animation", "anim_stand_man"),

		(position_set_z, pos5, ":eye_height"),

		# camera looks towards -z axis
		(position_rotate_x, pos5, -90),
		(position_rotate_z, pos5, 180),

		# now apply yaw and pitch
		(position_rotate_y, pos5, ":camera_yaw"),
		(position_rotate_x, pos5, ":camera_pitch"),
		(position_move_z, pos5, ":camera_distance", 0),
		(position_move_x, pos5, 5, 0),

		(try_begin),
			(ge, ":banner_mesh", 0),

			(init_position, pos1),
			(position_set_z, pos1, -1500),
			(position_set_x, pos1, 265),
			(position_set_y, pos1, 400),
			(position_transform_position_to_parent, pos3, pos5, pos1),
			(cur_tableau_add_mesh, ":banner_mesh", pos3, 400, 0),
		(try_end),
		
		(cur_tableau_add_troop, ":troop_no", pos2, ":animation" , 0),

		(cur_tableau_set_camera_position, pos5),

		(copy_position, pos8, pos5),
		(position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
		(position_rotate_z, pos8, 30), 
		(position_rotate_x, pos8, -60), 
		(cur_tableau_add_sun_light, pos8, 175,150,125),
	]),

	#script_add_troop_to_cur_tableau_for_character
	# INPUT: troop_no
	# OUTPUT: none
	("add_troop_to_cur_tableau_for_character",
	[
		(store_script_param, ":troop_no",1),

		(set_fixed_point_multiplier, 100),

		(cur_tableau_clear_override_items),
		(cur_tableau_set_override_flags, af_override_fullhelm),

		 
		(init_position, pos2),
		(cur_tableau_set_camera_parameters, 1, 4, 8, 10, 10000),

		(init_position, pos5),
		(assign, ":cam_height", 150),

		(assign, ":camera_distance", 360),
		(assign, ":camera_yaw", -15),
		(assign, ":camera_pitch", -18),
		(assign, ":animation", anim_stand_man),
		 
		(position_set_z, pos5, ":cam_height"),

		# camera looks towards -z axis
		(position_rotate_x, pos5, -90),
		(position_rotate_z, pos5, 180),

		# now apply yaw and pitch
		(position_rotate_y, pos5, ":camera_yaw"),
		(position_rotate_x, pos5, ":camera_pitch"),
		(position_move_z, pos5, ":camera_distance", 0),
		(position_move_x, pos5, 5, 0),

		(try_begin),
			(troop_is_hero, ":troop_no"),
			(cur_tableau_add_troop, ":troop_no", pos2, ":animation", -1),
		(else_try),
			(store_mul, ":random_seed", ":troop_no", 126233),
			(val_mod, ":random_seed", 1000),
			(val_add, ":random_seed", 1),
			(cur_tableau_add_troop, ":troop_no", pos2, ":animation", ":random_seed"),
		(try_end),

		(cur_tableau_set_camera_position, pos5),

		(copy_position, pos8, pos5),
		(position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
		(position_rotate_z, pos8, 30), 
		(position_rotate_x, pos8, -60), 
		(cur_tableau_add_sun_light, pos8, 175,150,125),
	]),

	#script_add_troop_to_cur_tableau_for_inventory
	# INPUT: troop_no
	# OUTPUT: none
	("add_troop_to_cur_tableau_for_inventory",
	[
		(store_script_param, ":troop_no",1),
		(store_mod, ":side", ":troop_no", 4), #side flag is inside troop_no value
		(val_div, ":troop_no", 4), #removing the flag bit
		(val_mul, ":side", 90), #to degrees

		(set_fixed_point_multiplier, 100),

		(cur_tableau_clear_override_items),
		 
		(init_position, pos2),
		(position_rotate_z, pos2, ":side"),
		(cur_tableau_set_camera_parameters, 1, 4, 6, 10, 10000),

		(init_position, pos5),
		(assign, ":cam_height", 105),

		(assign, ":camera_distance", 380),
		(assign, ":camera_yaw", -15),
		(assign, ":camera_pitch", -18),
		(assign, ":animation", anim_stand_man),
		 
		(position_set_z, pos5, ":cam_height"),

		# camera looks towards -z axis
		(position_rotate_x, pos5, -90),
		(position_rotate_z, pos5, 180),

		# now apply yaw and pitch
		(position_rotate_y, pos5, ":camera_yaw"),
		(position_rotate_x, pos5, ":camera_pitch"),
		(position_move_z, pos5, ":camera_distance", 0),
		(position_move_x, pos5, 5, 0),

		(try_begin),
			(troop_is_hero, ":troop_no"),
			(cur_tableau_add_troop, ":troop_no", pos2, ":animation", -1),
		(else_try),
			(store_mul, ":random_seed", ":troop_no", 126233),
			(val_mod, ":random_seed", 1000),
			(val_add, ":random_seed", 1),
			(cur_tableau_add_troop, ":troop_no", pos2, ":animation", ":random_seed"),
		(try_end),

		(cur_tableau_set_camera_position, pos5),

		(copy_position, pos8, pos5),
		(position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
		(position_rotate_z, pos8, 30), 
		(position_rotate_x, pos8, -60), 
		(cur_tableau_add_sun_light, pos8, 175,150,125),
	]),

	#script_add_troop_to_cur_tableau_for_profile
	# INPUT: troop_no
	# OUTPUT: none
	("add_troop_to_cur_tableau_for_profile",
	[
		(store_script_param, ":troop_no",1),

		(set_fixed_point_multiplier, 100),

		(cur_tableau_clear_override_items),
		 
		(cur_tableau_set_camera_parameters, 1, 4, 6, 10, 10000),

		(init_position, pos5),
		(assign, ":cam_height", 105),

		(assign, ":camera_distance", 380),
		(assign, ":camera_yaw", -15),
		(assign, ":camera_pitch", -18),
		(assign, ":animation", anim_stand_man),
		 
		(position_set_z, pos5, ":cam_height"),

		# camera looks towards -z axis
		(position_rotate_x, pos5, -90),
		(position_rotate_z, pos5, 180),

		# now apply yaw and pitch
		(position_rotate_y, pos5, ":camera_yaw"),
		(position_rotate_x, pos5, ":camera_pitch"),
		(position_move_z, pos5, ":camera_distance", 0),
		(position_move_x, pos5, 5, 0),

		(init_position, pos2),
		(try_begin),
			(troop_is_hero, ":troop_no"),
			(cur_tableau_add_troop, ":troop_no", pos2, ":animation", -1),
		(else_try),
			(store_mul, ":random_seed", ":troop_no", 126233),
			(val_mod, ":random_seed", 1000),
			(val_add, ":random_seed", 1),
			(cur_tableau_add_troop, ":troop_no", pos2, ":animation", ":random_seed"),
		(try_end),

		(cur_tableau_set_camera_position, pos5),

		(copy_position, pos8, pos5),
		(position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
		(position_rotate_z, pos8, 30), 
		(position_rotate_x, pos8, -60), 
		(cur_tableau_add_sun_light, pos8, 175,150,125),
	]),

	#script_add_troop_to_cur_tableau_for_party
	# INPUT: troop_no
	# OUTPUT: none
	("add_troop_to_cur_tableau_for_party",
	[
		(store_script_param, ":troop_no",1),
		(store_mod, ":hide_weapons", ":troop_no", 2), #hide_weapons flag is inside troop_no value
		(val_div, ":troop_no", 2), #removing the flag bit

		(set_fixed_point_multiplier, 100),

		(cur_tableau_clear_override_items),
		(try_begin),
		(eq, ":hide_weapons", 1),
		(cur_tableau_set_override_flags, af_override_fullhelm|af_override_head|af_override_weapons),
		(try_end),
		 
		(init_position, pos2),
		(cur_tableau_set_camera_parameters, 1, 6, 6, 10, 10000),

		(init_position, pos5),
		(assign, ":cam_height", 105),

		(assign, ":camera_distance", 450),
		(assign, ":camera_yaw", 15),
		(assign, ":camera_pitch", -18),
		(assign, ":animation", anim_stand_man),
		 
		(troop_get_inventory_slot, ":horse_item", ":troop_no", ek_horse),
		(try_begin),
			(gt, ":horse_item", 0),
			(eq, ":hide_weapons", 0),
			(cur_tableau_add_horse, ":horse_item", pos2, "anim_horse_stand", 0),
			(assign, ":animation", "anim_ride_0"),
			(assign, ":camera_yaw", 23),
			(assign, ":cam_height", 150),
			(assign, ":camera_distance", 550),
		(try_end),
		(position_set_z, pos5, ":cam_height"),

		# camera looks towards -z axis
		(position_rotate_x, pos5, -90),
		(position_rotate_z, pos5, 180),

		# now apply yaw and pitch
		(position_rotate_y, pos5, ":camera_yaw"),
		(position_rotate_x, pos5, ":camera_pitch"),
		(position_move_z, pos5, ":camera_distance", 0),
		(position_move_x, pos5, 5, 0),

		(try_begin),
			(troop_is_hero, ":troop_no"),
			(cur_tableau_add_troop, ":troop_no", pos2, ":animation", -1),
		(else_try),
			(store_mul, ":random_seed", ":troop_no", 126233),
			(val_mod, ":random_seed", 1000),
			(val_add, ":random_seed", 1),
			(cur_tableau_add_troop, ":troop_no", pos2, ":animation", ":random_seed"),
		(try_end),

		(cur_tableau_set_camera_position, pos5),

		(copy_position, pos8, pos5),
		(position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
		(position_rotate_z, pos8, 30), 
		(position_rotate_x, pos8, -60), 
		(cur_tableau_add_sun_light, pos8, 175,150,125),
	]),
	 
	# #######################################################################
	# #	Add your scripts here!
	# #						Go hog wild!
	# #######################################################################
	
	#("script_sort_agents_by_distance", agent_id, pos##),
	# This script produce and sort an array of all agents in a scene, based on distance from
	# the position provided by the scripter. This will skip over the anchor agent if provided
	# by leaving the agent_id parameter as 0, it will include all human agents in the scene
	#INPUT:
	#    Param 1: Anchor Agent (Leave as 0 to skip)
	#    Param 2: Anchor Position
	#OUTPUT:
	#    Reg45	: Total Agents (not counting Anchor Agent)
	# To sort, first store all agents in a scene and thier distance from anchor, unsorted. 
	# Iterate through all agent distances and swap the nearest with the lowest unsorted slot.
	# Then repeat until done. Store total agent count in reg45 to allow scripter to make
	# try_for_range loops using the total count as the upper limit.
	
   
	("sort_agents_by_distance",
		[
		# Input Variables
		(store_script_param, ":anchor_agent", 1),
		(store_script_param, ":anchor_pos", 2),
		
		# Initializing Other Variables
		(assign, ":array_slot", 0),						# Assign Minimum Slot Range
		(assign, ":agent_array", "trp_agent_array"),	# For Readability, create :LocalVar
		(assign, ":adist_array", "trp_adist_array"),	# For Readability, create :LocalVar

        (try_for_agents, ":agent"),													# Iterate through all agents
			(neq, ":agent", ":anchor_agent"),										# That is not the anchor
			(agent_is_human, ":agent"),												# And that are human
			
			(agent_get_position, pos45, ":agent"),									# Where are they?
			(get_distance_between_positions, ":distance", ":anchor_pos", pos45),	# How far is that from the anchor?
			
			(troop_set_slot, ":agent_array", ":array_slot", ":agent"),				# Store this agent in the first available blank slot
			(troop_set_slot, ":adist_array", ":array_slot", ":distance"),			# Store this distnace in the first available blank slot
			
			(val_add, ":array_slot", 1),											# Iterate that slot after this, moving to the next empty slot
		(try_end),
		
		(assign, reg45, ":array_slot"),
		
		(try_for_range, ":i", 0, ":array_slot"),									# Use the count from above to iterate through the array slots
			
			(assign, ":comp_dist", 999999),											# Large number to check against
			
			(try_for_range, ":j", ":i", ":array_slot"),								# Loops in Loops
				(troop_get_slot, ":adist", ":adist_array", ":j"),					# Check the distance stored in slot numbered :j
				(lt, ":adist", ":comp_dist"),										# If less than the previous comparison number
				(neq, ":adist", 0),													# And not a blank slot
				(assign, ":comp_dist", ":adist"),									# Have it replace the comparison number, and,
				(assign, ":nearest", ":j"),											# Remember what slot I used to be in
			(try_end),
			
			(troop_get_slot, ":nearest_agent", ":agent_array", ":nearest"),			# Take the remembered slot and pull the assoc. agent
			(troop_get_slot, ":nearest_distance", ":adist_array", ":nearest"),		# Take the remembered slot and pull the assoc. distance
			
			(troop_get_slot, ":swap_agent", ":agent_array", ":i"),					# Take the previously stored agent
			(troop_get_slot, ":swap_distance", ":adist_array", ":i"),				# Take the previously stored distance
				
			(troop_set_slot, ":agent_array", ":nearest", ":swap_agent"),			# Store this swapped agent to whereever the new nearest used to live
			(troop_set_slot, ":adist_array", ":nearest", ":swap_distance"), 		# Store this swapped distance to whereever the new nearest used to live
			
			(troop_set_slot, ":agent_array", ":i", ":nearest_agent"),				# Store this nearest agent as the newest member of the sorted part of the array
			(troop_set_slot, ":adist_array", ":i", ":nearest_distance"), 			# Store this nearest distance as the newest member of the sorted part of the array
		(try_end),
       
        ]
    ),
]